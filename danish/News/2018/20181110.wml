#use wml::debian::translation-check translation="3bec405a8dd51579843cbd6f2b5aa32ad6faa05f"
<define-tag pagetitle>Opdateret Debian 9: 9.6 udgivet</define-tag>
<define-tag release_date>2018-11-10</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debian-projektet er stolt over at kunne annoncere den sjette opdatering af 
dets stabile distribution, Debian <release> (kodenavn <q><codename></q>).
Denne opdatering indeholder primært rettelser af sikkerhedsproblemer i den 
stabile udgave, sammen med nogle få rettelser af alvorlige problemer.  
Sikkerhedsbulletiner er allerede udgivet separat og der vil blive refereret til 
dem, hvor de er tilgængelige.</p>

<p>Bemærk at denne opdatering ikke er en ny udgave af Debian GNU/Linux
<release>, den indeholder blot opdateringer af nogle af de medfølgende pakker.  
Der er ingen grund til at smide gamle <q><codename></q>-medier væk.  Efter en 
installering, kan pakkerne opgradere til de aktuelle versioner ved hjælp af et 
ajourført Debian-filspejl.</p>

<p>Dem der hyppigt opdaterer fra security.debian.org, behøver ikke at opdatere 
ret mange pakker, og de fleste opdateringer fra security.debian.org er indeholdt 
i denne opdatering.</p>

<p>Nye installeringsfilaftryk vil snart være tilgængelige fra de sædvanlige 
steder.</p>

<p>Online-opdatering til denne revision gøres normalt ved at lade 
pakkehåndteringssystemet pege på et af Debians mange HTTP-filspejle. En 
omfattende liste over filspejle er tilgængelig på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>



<h2>Forskellige fejlrettelser</h2>

<p>Denne opdatering til den stabile udgave tilføjer nogle få vigtige rettelser 
til følgende pakker:</p>

<table border=0>
<tr><th>Pakke</th>					<th>Årsag</th></tr>
<correction accerciser					"Retter tilgang til elementer uden en compositor; retter Python-konsol; tilføjer manglende afhængighed af python3-xlib">
<correction apache2					"mod_http2: Retter lammelsesgreb i form af workerudmattelse [CVE-2018-1333] og ved løbende SETTINGS [CVE-2018-11763]; mod_proxy_fcgi: Retter segmenteringsfejl">
<correction base-files					"Opdaterer /etc/debian_version til denne punktopdatering">
<correction brltty					"Retter polkit-autentifikation">
<correction canna					"Retter filkonlikt mellem canna-dbgsym og canna-utils-dbgsym">
<correction cargo					"Ny pakke til understøttelse af Firefox ESR60-opbygning">
<correction clamav					"Ny opstrømsudgave; retter HWP-heltalsoverløb, uendelig løkke-sårbarhed [CVE-2018-0360]; retter problem med tjek af PDF-objektlængde, bruger alt for lang tid på at fortolke relativ lille fil [CVE-2018-0361]; nye opstrømsversion; retter lammelsesangrebsproblem [CVE-2018-15378]; retter uendelig løkke i dpkg-reconfigure">
<correction confuse					"Retter en læsning udenfor grænserne i trim_whitespace [CVE-2018-14447]">
<correction debian-installer				"Opdaterer til -8-kerne-ABI">
<correction debian-installer-netboot-images		"Genopbygger til denne punktopdatering">
<correction dnsmasq					"trust-anchors.conf: Medtager seneste DNS-trustanchor KSK-2017">
<correction dom4j					"Retter XML-indsprøjtningsangreb [CVE-2018-1000632]; kompile med source/target 1.5 for at rette kompileringsproblem med String.format">
<correction dpdk					"Ny stabil opstrømsudgave">
<correction dropbear					"Retter brugeroptællingssårbarhed [CVE-2018-15599]">
<correction easytag					"Retter OGG-korruption">
<correction enigmail					"Tilføjer kompatibilitet med nyere versioner af Thunderbird">
<correction espeakup					"espeakup.service: Indlæser automatisk speakup_soft ved start af dæmon">
<correction fastforward					"Retter segmenteringsfejl på 64 bit-arkitekturer">
<correction firetray					"Tilføjer kompatibilitet med nyere versioner af Thunderbird">
<correction firmware-nonfree				"Retter sikkerhedsproblemer i Broadcom-wififirmware [CVE-2016-0801 CVE-2017-0561 CVE-2017-9417 CVE-2017-13077 CVE-2017-13078 CVE-2017-13079 CVE-2017-13080 CVE-2017-13081]; gentilføjer transitionspakker til firmware-{adi,ralink}">
<correction fofix-dfsg					"Retter fejl ved start">
<correction fuse					"Hvidlister autofs og FAT som gyldige mountpointfilsystemer">
<correction ganeti					"Verificer på korrekt vis SSL-certifikater under VM-eksport; signér genererede certifikater ved hjælp af SHA256 i stedet for SHA1; gør bash-completions automatisk indlæsbare">
<correction globus-gsi-credential			"Retter problem med voms-proxy og openssl 1.1">
<correction gnupg2					"Sikkerhedsrettelser; tilbagefører funktionalitet krævet af ny enigmail">
<correction gnutls28					"Retter sikkerhedsproblemer [CVE-2018-10844 CVE-2018-10845]">
<correction gphoto2-cffi				"Får python3-gphoto2cffi til at fungere igen">
<correction grub2					"grub-mknetdir: Tilføjer understøttelse af ARM64 EFI; ændrer standardmetoden for TSC-kalibrering til pmtimer på EFI-systemer">
<correction hdparm					"Aktiver kun APM på diske, som udstiller det">
<correction https-everywhere				"Tilbagefører ny opstrømsversion, af hensyn til kompatibilitet med Firefox ESR 60">
<correction i3-wm					"Retter nedbrud ved genstart når der anvendes marks">
<correction iipimage					"Retter Apache-opsætning">
<correction jhead					"Retter sikkerhedsproblemer [CVE-2018-17088 CVE-2018-16554]">
<correction lastpass-cli				"Tilbagefører hårdkodede certifikatpins fra lastpass-cli 1.3.1 for at afspejle ændringer i den hostede Lastpass.com-tjeneste">
<correction ldap2zone					"Retter uendelig løkke ved tjek af zoneløbenummer">
<correction libcgroup					"Retter logfiler som var tilgængelige (og skrivbare) for alle [CVE-2018-14348]">
<correction libclamunrar				"Ny opstrømsugave">
<correction libdap					"Retter indhold i libdap-doc">
<correction libdatetime-timezone-perl			"Opdaterer medfølgende data">
<correction libgd2					"Bmp: Tjek returværdi i gdImageBmpPtr [CVE-2018-1000222]; retter potentiel uendelig løkke i gdImageCreateFromGifCtx [CVE-2018-5711]">
<correction libmail-deliverystatus-bounceparser-perl	"Fjerner ikke-distribuerbare eksempler på spam og virus">
<correction libmspack					"Retter skrivning udenfor grænserne [CVE-2018-18584] og accept af <q>blanke</q> filnavne [CVE-2018-18585]">
<correction libopenmpt					"Retter <q>up11: Out-of-bounds read loading IT / MO3 files with many pattern loops</q> [CVE-2018-10017]">
<correction libseccomp					"Tilføjer understøttelse af Linux 4.9-syscalls: preadv2, pwritev2, pkey_mprotect, pkey_alloc og pkey_free; tilføjer understøttelse af statx">
<correction libtirpc					"rendezvous_request: Kontrollerer returværdi fra makefd_xprt [CVE-2018-14622]">
<correction libx11					"Retter flere sikkerhedsproblemer [CVE-2018-14598 CVE-2018-14599 CVE-2018-14600]">
<correction libxcursor					"Retter et lammelsesangreb eller potentiel kodeudførsel gennem et en-byte-heapoverløb [CVE-2015-9262]">
<correction libxml-stream-perl				"Leverer en standard-CA-sti">
<correction libxml-structured-perl			"Tilføjer manglende build- og runtimeafhængighed af libxml-parser-perl">
<correction linux					"Xen: Retter bootregression i PV-domæner; xen-netfront: Retter regressioner; ext4: retter falske negative *og* falske positive i ext4_check_descriptors(); udeb: Tilføjer virtio_console til virtio-modules; cdc_ncm: undgår padding ud over slutningen af skb; tilbageruller <q>sit: genindlæs iphdr i ipip6_rcv</q>; ny opstrømsudgave">
<correction lxcfs					"Tilbageruller oppetidsvirtualisering, retter processtarttider">
<correction magicmaze					"Afhænger af fonts-isabella nu hvor ttf-isabella er en virtuell pakke">
<correction mailman					"Retter sårbarhed i forbindelse med indspøjtning af vilkårlig tekst i Mailman-CGI'er [CVE-2018-13796]">
<correction multipath-tools				"Undgår deadlock i udev-triggere">
<correction nagstamon					"Løser problem med IcingaWeb2 Basic-autentifikation">
<correction network-manager				"libnm: Retter adgang til aktiverede og optalte egenskaber; retter heapskrivning udenfor grænserne i håndteringen af dhcpv6-valgmulighed [CVE-2018-15688] og forskellige andre problemer i den sd-network-baserede dhcp=internal-plugin">
<correction network-manager-applet			"libnma/pygobject: libnma/NMA skal anvende libnm/NM i stedet for forældede biblioteker">
<correction ola						"Retter slåfejl i /etc/init.d/rdm_test_server; retter jquery-filnavn i rdm-testservers statiske HTML-filer">
<correction opensc					"Retter grænseløs rekursion samt flere læsninger eller skrivninger udenfor grænserne [CVE-2018-16391 CVE-2018-16392 CVE-2018-16393 CVE-2018-16418 CVE-2018-16419 CVE-2018-16420 CVE-2018-16421 CVE-2018-16422 CVE-2018-16423 CVE-2018-16424 CVE-2018-16425 CVE-2018-16426 CVE-2018-16427]">
<correction pkgsel					"Installerer nye afhængigheder når safe-upgrade (standard) er valgt">
<correction publicsuffix				"Opdaterer medfølgende data">
<correction python-django				"Understøt som standard Spatialite &gt;= 4.2">
<correction python-imaplib2				"Installer det korrekte modul til Python 3; anvend ikke TIMEOUT_MAX">
<correction rustc					"Aktiverer opbygning på flere arkitekturer: arm64, armel, armhf, i386, ppc64el, s390x">
<correction sddm					"Efterkommer PAM's ambient suppleringsgrupper; tilføjer manglende håndtering af utmp/wtmp/btmp">
<correction serf					"Retter NULL-pointerdereference">
<correction soundconverter				"Retter opus vbr-indstilling">
<correction spamassassin				"Ny opstrømsudgave; retter lammelsesangreb [CVE-2017-15705], fjernudførsel af kode [CVE-2018-11780], kodeindsprøjtning [CVE-2018-11781] og usikker anvendelse af <q>.</q> i @INC [CVE-2016-1238]; retter håndtering af spamd-service ved pakkeopgraderinger">
<correction spice-gtk					"Retter bufferoverløb i fleksibelt array [CVE-2018-10873]">
<correction sqlcipher					"Undgår nedbrud ved åbning af en fil">
<correction subversion					"Retter en regression opstået ved rettelserne af SHA1-kollisioner, hvor commits fejlede på ukorrekt vis med en <q>Filesystem is corrupt</q>-fejlmeddelelse, hvis deltalængden er et multiplum af 16K">
<correction systemd					"networkd: Lad ikke manager_connect_bus() fejle hvis dbus endnu ikke er aktiv; dhcp6: Sørg for at vi har plads nok til DHCP6-optionheader [CVE-2018-15688]">
<correction systraq					"Vend logikken om for at afslutte med succes i tilfælde af at /e/s/Makefile mangler">
<correction tomcat-native				"Retter problem med OSCP-responder, som gjorde det muligt for brugere at autentificere sig med tilbagetrukne certifikater, når der benyttes mutual TLS [CVE-2018-8019 CVE-2018-8020]">
<correction tor						"Mappeautoritetsændringer: pensionerer <q>Bifroest</q>-broautoriteten, til fordel for <q>Serge</q>; tilføjer en IPv6-adresse til <q>dannenberg</q>-mappeautoriteten">
<correction tzdata					"Nye opstrømsudgave">
<correction ublock-origin				"Tilbagefører ny opstrømsversion, af hensyn til kompatibility med Firefox ESR 60">
<correction unbound					"Retter sårbarhed i behandlingen af syntentiserede wildcard-NSEC-poster [CVE-2017-15105]">
<correction vagrant					"Understøtter VirtualBox 5.2">
<correction vmtk					"python-vmtk: Tilføjer den manglende afhængighed af python-vtk6">
<correction wesnoth-1.12				"Tillad ikke indlæsning af lua-bytecode gennem load/dofile [CVE-2018-1999023]">
<correction wpa						"Ignorer uautentificeret, krypteret EAPOL-Key-data [CVE-2018-14526]">
<correction x11vnc					"Retter to bufferoverløb">
<correction xapian-core					"Retter glass-backendfejl med langlivede markører i en tabel med WritableDatabase, hvilket på ukorrekt vis kunne føre til kastelse af DatabaseCorruptError, når databasen faktisk var i orden">
<correction xmotd					"Undgår nedbrud med hardening-flags">
<correction xorg-server					"GLX: Vælg ikke sRGB-opsætning for 32 bit-RGBA visual - retter forskellige blendingproblemer med kwin og Mesa &gt;= 18.0 (dvs. Mesa fra stretch-backports)">
<correction zutils					"Retter et bufferoverløb i zcat [CVE-2018-1000637]">
</table>


<h2>Sikkerhedsopdateringer</h2>

<p>Denne revision tilføjer følgende sikkerhedsopdateringer til den stabile 
udgave.  Sikkerhedsteamet har allerede udgivet bulletiner for hver af de nævnte
opdateringer:</p>

<table border=0>
<tr><th>Bulletin-id</th>  <th>Pakke(r)</th></tr>
<dsa 2017 4074 imagemagick>
<dsa 2018 4103 chromium-browser>
<dsa 2018 4182 chromium-browser>
<dsa 2018 4237 chromium-browser>
<dsa 2018 4242 ruby-sprockets>
<dsa 2018 4243 cups>
<dsa 2018 4244 thunderbird>
<dsa 2018 4245 imagemagick>
<dsa 2018 4246 mailman>
<dsa 2018 4247 ruby-rack-protection>
<dsa 2018 4248 blender>
<dsa 2018 4249 ffmpeg>
<dsa 2018 4250 wordpress>
<dsa 2018 4251 vlc>
<dsa 2018 4252 znc>
<dsa 2018 4253 network-manager-vpnc>
<dsa 2018 4254 slurm-llnl>
<dsa 2018 4256 chromium-browser>
<dsa 2018 4257 fuse>
<dsa 2018 4258 ffmpeg>
<dsa 2018 4260 libmspack>
<dsa 2018 4261 vim-syntastic>
<dsa 2018 4262 symfony>
<dsa 2018 4263 cgit>
<dsa 2018 4264 python-django>
<dsa 2018 4265 xml-security-c>
<dsa 2018 4266 linux>
<dsa 2018 4267 kamailio>
<dsa 2018 4268 openjdk-8>
<dsa 2018 4269 postgresql-9.6>
<dsa 2018 4270 gdm3>
<dsa 2018 4271 samba>
<dsa 2018 4272 linux>
<dsa 2018 4273 intel-microcode>
<dsa 2018 4274 xen>
<dsa 2018 4275 keystone>
<dsa 2018 4276 php-horde-image>
<dsa 2018 4277 mutt>
<dsa 2018 4278 jetty9>
<dsa 2018 4279 linux>
<dsa 2018 4279 linux-latest>
<dsa 2018 4280 openssh>
<dsa 2018 4281 tomcat8>
<dsa 2018 4282 trafficserver>
<dsa 2018 4283 ruby-json-jwt>
<dsa 2018 4284 lcms2>
<dsa 2018 4285 sympa>
<dsa 2018 4286 curl>
<dsa 2018 4287 firefox-esr>
<dsa 2018 4288 ghostscript>
<dsa 2018 4289 chromium-browser>
<dsa 2018 4290 libextractor>
<dsa 2018 4291 mgetty>
<dsa 2018 4292 kamailio>
<dsa 2018 4293 discount>
<dsa 2018 4294 ghostscript>
<dsa 2018 4295 thunderbird>
<dsa 2018 4296 mbedtls>
<dsa 2018 4297 chromium-browser>
<dsa 2018 4298 hylafax>
<dsa 2018 4299 texlive-bin>
<dsa 2018 4300 libarchive-zip-perl>
<dsa 2018 4301 mediawiki>
<dsa 2018 4302 openafs>
<dsa 2018 4303 okular>
<dsa 2018 4304 firefox-esr>
<dsa 2018 4305 strongswan>
<dsa 2018 4306 python2.7>
<dsa 2018 4307 python3.5>
<dsa 2018 4308 linux>
<dsa 2018 4309 strongswan>
<dsa 2018 4310 firefox-esr>
<dsa 2018 4311 git>
<dsa 2018 4312 tinc>
<dsa 2018 4313 linux>
<dsa 2018 4314 net-snmp>
<dsa 2018 4315 wireshark>
<dsa 2018 4316 imagemagick>
<dsa 2018 4317 otrs2>
<dsa 2018 4318 moin>
<dsa 2018 4319 spice>
<dsa 2018 4320 asterisk>
<dsa 2018 4321 graphicsmagick>
<dsa 2018 4322 libssh>
<dsa 2018 4323 drupal7>
<dsa 2018 4324 firefox-esr>
<dsa 2018 4325 mosquitto>
<dsa 2018 4326 openjdk-8>
<dsa 2018 4327 thunderbird>
<dsa 2018 4328 xorg-server>
<dsa 2018 4329 teeworlds>
<dsa 2018 4331 curl>
</table>


<h2>Fjernede pakker</h2>

<p>Følgende pakker er blevet fjernet på grund af omstændigheder uden for vores 
kontrol:</p>

<table border=0>
<tr><th>Pakke</th>				<th>Årsag</th></tr>
<correction adblock-plus-element-hiding-helper	"Ikke kompatibel med nyere versioner af firefox-esr">
<correction all-in-one-sidebar 			"Ikke kompatibel med nyere versioner af firefox-esr">
<correction autofill-forms			"Ikke kompatibel med nyere versioner af firefox-esr">
<correction automatic-save-folder		"Ikke kompatibel med nyere versioner af firefox-esr">
<correction classic-theme-restorer		"Ikke kompatibel med nyere versioner af firefox-esr">
<correction colorfultabs			"Ikke kompatibel med nyere versioner af firefox-esr">
<correction custom-tab-width			"Ikke kompatibel med nyere versioner af firefox-esr">
<correction dactyl				"Ikke kompatibel med nyere versioner af firefox-esr">
<correction downthemall				"Ikke kompatibel med nyere versioner af firefox-esr">
<correction dvips-fontdata-n2bk			"Tom pakke">
<correction firebug				"Ikke kompatibel med nyere versioner af firefox-esr">
<correction firegestures			"Ikke kompatibel med nyere versioner af firefox-esr">
<correction firexpath				"Ikke kompatibel med nyere versioner af firefox-esr">
<correction flashgot				"Ikke kompatibel med nyere versioner af firefox-esr">
<correction form-history-control		"Ikke kompatibel med nyere versioner af firefox-esr">
<correction foxyproxy				"Ikke kompatibel med nyere versioner af firefox-esr">
<correction gitlab				"Åbne sikkerhedsproblemer, svært at tilbageføre rettelser">
<correction greasemonkey			"Ikke kompatibel med nyere versioner af firefox-esr">
<correction intel-processor-trace		"[s390x] Kun nyttig på Intel-arkitekturer">
<correction itsalltext				"Ikke kompatibel med nyere versioner af firefox-esr">
<correction knot-resolver			"Sikkerhedsproblemer, svært at tilbageføre rettelser">
<correction lightbeam				"Ikke kompatibel med nyere versioner af firefox-esr">
<correction livehttpheaders			"Ikke kompatibel med nyere versioner af firefox-esr">
<correction lyz					"Ikke kompatibel med nyere versioner af firefox-esr">
<correction npapi-vlc				"Ikke kompatibel med nyere versioner af firefox-esr">
<correction nukeimage				"Ikke kompatibel med nyere versioner af firefox-esr">
<correction openinbrowser			"Ikke kompatibel med nyere versioner af firefox-esr">
<correction perspectives-extension		"Ikke kompatibel med nyere versioner af firefox-esr">
<correction pwdhash				"Ikke kompatibel med nyere versioner af firefox-esr">
<correction python-facebook			"Defekt på grund af opstrømsændringer">
<correction python-tvrage			"Nytteløs efter lukningen af tvrage.com">
<correction reloadevery				"Ikke kompatibel med nyere versioner af firefox-esr">
<correction sage-extension			"Ikke kompatibel med nyere versioner af firefox-esr">
<correction scrapbook				"Ikke kompatibel med nyere versioner af firefox-esr">
<correction self-destructing-cookies		"Ikke kompatibel med nyere versioner af firefox-esr">
<correction spdy-indicator			"Ikke kompatibel med nyere versioner af firefox-esr">
<correction status-4-evar			"Ikke kompatibel med nyere versioner af firefox-esr">
<correction stylish				"Ikke kompatibel med nyere versioner af firefox-esr">
<correction tabmixplus				"Ikke kompatibel med nyere versioner af firefox-esr">
<correction tree-style-tab			"Ikke kompatibel med nyere versioner af firefox-esr">
<correction ubiquity-extension			"Ikke kompatibel med nyere versioner af firefox-esr">
<correction uppity				"Ikke kompatibel med nyere versioner af firefox-esr">
<correction useragentswitcher			"Ikke kompatibel med nyere versioner af firefox-esr">
<correction video-without-flash			"Ikke kompatibel med nyere versioner af firefox-esr">
<correction webdeveloper			"Ikke kompatibel med nyere versioner af firefox-esr">
<correction xul-ext-monkeysphere		"Ikke kompatibel med nyere versioner af firefox-esr">
</table>


<h2>Debian Installer</h2>

Installeringsprogrammet er opdateret for at medtage rettelser indført i stable, 
i denne punktopdatering.


<h2>URL'er</h2>

<p>Den komplette liste over pakker, som er ændret i forbindelse med denne 
revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuelle stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Foreslåede opdateringer til den stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Oplysninger om den stabile distribution (udgivelsesbemærkninger, fejl, 
osv.):</p>

<div class="center">
  <a href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sikkerhedsannonceringer og -oplysninger:</p>

<div class="center">
  <a href="$(HOME)/security/">https://security.debian.org/</a>
</div>


<h2>Om Debian</h2>

<p>Debian-projektet er en organisation af fri software-udviklere som frivilligt
bidrager med tid og kræfter, til at fremstille det helt frie styresystem Debian
GNU/Linux.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt; eller kontakt holdet bag den stabile udgave på 
&lt;debian-release@debian.org&gt;.</p>
