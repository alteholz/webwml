#use wml::debian::template title="GNU GENERAL PUBLIC LICENSE &mdash; GNU ALGEMENE PUBLIEKE LICENTIE" NOCOPYRIGHT="true"
#use wml::debian::translation-check translation="d109ac4197d816202547b41d15ccf7b9e9e35b8e"

<hr />
<p>
<b>Note</b>: This is an unofficial translation of the GNU General Public License into Dutch. It was not published by the Free Software Foundation, and does not legally state the distribution terms for software that uses the GNU GPL &mdash; only the original <a href="gpl2.en.html">English text</a> of the GNU GPL does that. However, we hope that this translation will help Dutch speakers to better understand the GNU GPL.
</p>
<p>
<b>Opmerking</b>: dit is een niet-officiële vertaling naar het Nederlands van de GNU General Public License. Deze vertaling is niet gemaakt door de Free Software Foundation en bevat niet de juridisch bindende distributievoorwaarden voor software die gebruik maakt van de GNU GPL.  &mdash; <em>Enkel</em> de originele <a href="gpl2.en.html">Engelse tekst</a> van de GNU GPL is juridisch bindend. Wij hopen echter dat deze vertaling Nederlandstaligen zal helpen de GNU GPL beter te begrijpen.
</p>
<p>
<small>Deze vertaling is geen origineel werk, maar maakt zoveel mogelijk gebruik van reeds geleverd vertaalwerk. In eerste instantie wordt dankbaar gebruik gemaakt van de niet-officiële vertaling van versie 2 van de licentie die te vinden is op <url http://users.skynet.be/xterm/gpld.txt>. Waar de tekst tussen de versies 2 en 3 van de GPL ongewijzigd bleef, gingen we ook te rade bij de Nederlandse vertaling door Bart Beuving en Maurits Westerik van GPLv3 die u vindt op <url https://bartbeuving.files.wordpress.com/2008/07/gpl-v3-nl-101.pdf>.</small>
</p>
<hr />

<p><strong>
Versie 2, juni 1991
</strong></p>

<pre>
Copyright (C) 1989, 1991 Free Software Foundation, Inc.  
51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

Het is iedereen toegestaan om letterlijke kopieën van deze
licentievoorwaarden te maken en deze te verspreiden. Het wijzigen
van deze licentievoorwaarden is niet toegestaan.

</pre>

<h3><a name="preamble"></a><a name="SEC2">Preambule</a></h3>

<p>
  De licenties voor de meeste software zijn ontworpen om u het recht te ontnemen om deze software te delen en te wijzigen. In tegenstelling hiermee is de GNU Algemene Publieke Licentie bedoeld om u het recht te garanderen op het delen en wijzigen van vrije software &mdash; om ervoor te zorgen dat het vrije software blijft voor alle gebruikers. Deze Algemene Publieke Licentie is van toepassing op de meeste software van de Free Software Foundation en op elk ander programma waarvan de auteurs zich engageren om ze te gebruiken. (Sommige software van de Free Software Foundation valt evenwel onder de GNU Library General Public License &mdash; de GNU Algemene Publieke Licentie voor Softwarebibliotheken.) U kunt deze licentie ook op uw programma’s van toepassing verklaren.
</p>

<p>
  Wanneer wij het hebben over vrije software, dan bedoelen wij vrijheid, niet kostprijs. Onze GNU Algemene Publieke Licenties zijn geschreven om ervoor te zorgen dat u de vrijheid heeft om kopieën van vrije software te verspreiden (en voor deze dienst een vergoeding te vragen als u dat wenst), dat u de broncode ontvangt of ze desgewenst kunt krijgen, dat u de software kunt wijzigen of stukken ervan kunt gebruiken in nieuwe vrije programma’s en dat u weet dat u deze dingen kunt doen.
</p>

<p>
  Om uw rechten te beschermen, moeten wij beperkingen instellen die anderen verbieden u deze rechten te ontzeggen of u te vragen afstand te doen van deze rechten. Deze restricties brengen enkele verantwoordelijkheden mee voor u als u kopieën van de software verspreidt, of als u deze software wijzigt.

</p>

<p>
  Bijvoorbeeld, als u kopieën van een dergelijk programma verspreidt, of het nu gratis is of tegen een vergoeding, dan moet u aan de ontvangers alle rechten geven die u zelf heeft. U moet zorgen dat ook zij de broncode ontvangen of kunnen verkrijgen. Ook moet u ze deze voorwaarden tonen zodat zij hun rechten kennen.
</p>

<p>
  Wij beschermen uw rechten in twee stappen: (1) auteursrecht plaatsen op de software, en (2) u deze licentie aanbieden die u juridisch gezien toestemming geeft om deze software te kopiëren, verspreiden en/of te wijzigen.
</p>

<p>
  Ter bescherming van elke auteur en van onszelf, willen we tevens ook zeker zijn dat iedereen begrijpt dat er geen garanties op deze vrije software zitten. Als de software door iemand anders gewijzigd en verspreid wordt, willen we dat de ontvangers van deze software weten dat zij niet de originele versie ervan hebben, zodat eventuele problemen die anderen introduceerden niet onterecht de reputatie van de originele auteur zouden beschadigen.
</p>

<p>
  Tot slot, elk vrij programma wordt voortdurend bedreigd door softwareoctrooien. Wij willen het gevaar vermijden dat verdelers van een vrij programma zelf octrooien zouden verwerven en op die manier dat programma feitelijk <q>gesloten software</q> zouden maken. Om dit te voorkomen stellen we duidelijk dat een eventueel octrooi in licentie moet gegeven worden met het oog op een vrij gebruik door iedereen of helemaal niet in licentie gegeven moet worden.
</p>

<p>
  De precieze bepalingen en voorwaarden inzake kopiëren, verspreiden en wijzigen volgen hierna.
</p>


<h3><a name="terms"></a><a name="SEC3">BEPALINGEN EN VOORWAARDEN INZAKE KOPIËREN, VERSPREIDEN EN WIJZIGEN</a></h3>


<a name="section0"></a><p>
<strong>0.</strong>

 Deze licentie is van toepassing op elk programma of ander werk dat een vermelding bevat van de copyrighthouder, waarin staat dat het mag verspreid worden onder de bepalingen van deze Algemene Publieke Licentie. Als hierna "Programma" gebruikt wordt, verwijst dit naar elk dergelijk programma of werk en een "op het Programma gebaseerd werk" betekent het Programma of elk afgeleid werk volgens de copyrightwetgeving: dit wil zeggen, een werk dat het Programma bevat of een gedeelte ervan, ofwel letterlijk of gewijzigd en/of naar een andere taal vertaald. (In wat hierna volgt is vertaling zonder beperking inbegrepen in de term "wijziging".) Elke licentiehouder wordt aangesproken als "u".
</p>

<p>
Andere handelingen dan kopiëren, verspreiden en wijzigen vallen niet onder deze Licentie. Hiervoor is deze niet bedoeld. Er staan geen beperkingen op het uitvoeren van het Programma en de uitvoer die het Programma produceert, valt enkel onder deze licentie indien de inhoud van die uitvoer een op het Programma gebaseerd werk vormt (los van het feit dat het gerealiseerd werd door het Programma uit te voeren). Of dit het geval is hangt af van wat het Programma doet.
</p>

<a name="section1"></a><p>
<strong>1.</strong>
 U mag letterlijke kopieën van de broncode van het Programma zoals je die zelf gekregen hebt, kopiëren en verspreiden op elk medium, op voorwaarde dat u op duidelijk waarneembare en passende wijze op elke kopie een toepasselijke copyright-kennisgeving aanbrengt en een niet-aansprakelijkheidsverklaring, alle kennisgevingen die naar deze Licentie en naar de niet-aansprakelijkheidsverklaring verwijzen intact laat, en alle ontvangers van het Programma samen met het Programma een kopie van deze Licentie geeft.
</p>

<p>
U mag een vergoeding vragen voor de materiële handeling van het overmaken van een kopie en u mag naar eigen goeddunken garanties bieden in ruil voor een vergoeding.

</p>

<a name="section2"></a><p>
<strong>2.</strong>
 U mag uw kopie of kopieën van het Programma of elk deel ervan wijzigen, en aldus een op het Programma gebaseerd werk vormen, en u mag dergelijke wijzigingen kopiëren en verspreiden onder de bepalingen van Paragraaf 1 hierboven, indien U ook aan al deze voorwaarden voldoet:
</p>

<dl>
  <dt>&nbsp;</dt>
    <dd>
      <strong>a)</strong>

      U moet ervoor zorgen dat de gewijzigde bestanden een duidelijke vermelding bevatten van het feit dat U het bestand gewijzigd hebt en van de datum van elke wijziging.
    </dd>
  <dt>&nbsp;</dt>
    <dd>
      <strong>b)</strong>
      U moet elk werk dat U verspreidt of publiceert en dat het Programma geheel of gedeeltelijk bevat of van dat Programma of van een deel ervan afgeleid is, in zijn geheel onder de bepalingen van deze Licentie kosteloos in licentie geven aan alle derde partijen.
    </dd>
  <dt>&nbsp;</dt>
    <dd>

      <strong>c)</strong>
      Indien het gewijzigde programma normaal gezien op een interactieve manier commando's verwerkt wanneer het uitgevoerd wordt, dan moet u ervoor zorgen dat, als het voor een dergelijk interactief gebruik in zijn meest eenvoudige vorm opgestart wordt, het een kennisgeving afdrukt of weergeeft met een passende copyrightvermelding en met de vermelding dat er geen garantie geboden wordt (of anders, dat U een garantie voorziet) en dat gebruikers het programma mogen verspreiden onder deze voorwaarden en waarin de gebruiker geïnformeerd wordt over hoe hij een kopie van deze Licentie kan bekijken. (Uitzondering : als het Programma zelf interactief is maar normaal geen dergelijke kennisgeving afdrukt, dan moet uw op het Programma gebaseerd werk evenmin die kennisgeving afdrukken.)
    </dd>
</dl>

<p>
Deze vereisten zijn van toepassing op het werk in zijn geheel. Als duidelijke onderdelen van dat werk niet afgeleid zijn van het Programma en redelijkerwijs beschouwd kunnen worden als op zich staande onafhankelijke en afzonderlijke werken, dan is deze Licentie en zijn bepalingen niet van toepassing op die onderdelen als U die als aparte werken verspreidt. Maar als u diezelfde onderdelen verspreidt als deel van een geheel dat een op het Programma gebaseerd werk is, dan moet de verspreiding van het geheel gebeuren in overeenstemming met de bepalingen van deze Licentie, waarvan de vergunningen voor andere licentiehouders betrekking hebben op het volledige geheel, en dus op elk onderdeel, ongeacht wie het schreef.
</p>

<p>
Het is dus niet de bedoeling van deze paragraaf om aanspraak te maken op rechten op werk dat geheel door uzelf geschreven is of uw rechten op dat werk te betwisten. Het is eerder de bedoeling om het recht uit te oefenen op controle over de verspreiding van afgeleide of collectieve werken gebaseerd op het Programma.
</p>

<p>

Daarenboven geldt dat het louter samenbrengen van een ander werk dat niet op het Programma gebaseerd is met het Programma (of met een op het Programma gebaseerd werk) op een opslagmedium of een verspreidingsmedium, dat ander werk niet doet vallen onder de bepalingen van deze Licentie.
</p>

<a name="section3"></a><p>
<strong>3.</strong>
 U mag het Programma (of een volgens paragraaf 2 erop gebaseerd werk) verspreiden en kopiëren in objectcode of uitvoerbare vorm onder de bepalingen van bovenstaande paragrafen 1 en 2 op voorwaarde dat U ook aan een van de volgende voorwaarden voldoet:
</p>

<!-- we use this doubled UL to get the sub-sections indented, -->
<!-- while making the bullets as unobvious as possible. -->

<dl>
  <dt>&nbsp;</dt>
    <dd>

      <strong>a)</strong>
      Dat u de volledige bijbehorende mechanisch afleesbare broncode eraan toevoegt, die verspreid moet worden onder de bepalingen van de bovenstaande paragrafen 1 en 2 op een medium dat gebruikelijk is voor het uitwisselen van software, of
    </dd>
  <dt>&nbsp;</dt>
    <dd>
      <strong>b)</strong>
      Dat u een schriftelijk aanbod toevoegt, dat minstens drie jaar geldig blijft, om aan elke derde een volledige mechanisch afleesbare kopie van de bijbehorende broncode te verstrekken onder de bepalingen van de bovenstaande paragrafen 1 en 2 op een medium dat gebruikelijk is voor het uitwisselen van software tegen een vergoeding die niet hoger ligt dan de materiële kost voor het verspreiden van de broncode, of
    </dd>
  <dt>&nbsp;</dt>

    <dd>
      <strong>c)</strong>
      Dat u de informatie toevoegt die u ontving met betrekking tot het aanbod om de bijbehorende broncode te verdelen. (Dit alternatief is enkel toegestaan voor een niet-commerciële verspreiding en enkel als u het programma ontving in objectcode of in uitvoerbare vorm met een dergelijk aanbod in overeenstemming met de bovenstaande subparagraaf b.)
    </dd>
</dl>

<p>
Onder de broncode van een werk wordt die vorm van het werk verstaan waarin het bij voorkeur bewerkt wordt. Voor een uitvoerbaar werk betekent volledige broncode alle broncode van alle modules waaruit het werk bestaat, plus alle bestanden die een eventuele erbij horende interface definiëren, plus de scripts voor het compileren en het installeren van het uitvoerbare programma. Bij wijze van bijzondere uitzondering geldt echter dat in de gedistribueerde broncode niets moet opgenomen worden dat normaal verspreid wordt (in broncode of in uitvoerbare vorm) via de hoofdcomponenten (compiler, kernel, enz.) van het besturingssysteem waaronder het programma uitgevoerd wordt, tenzij die component zelf bij het uitvoerbare bestand hoort.
</p>

<p>
Als de verspreiding van het uitvoerbare bestand of de objectcode gebeurt door het bieden van de mogelijkheid om deze te kopiëren van een bepaalde plaats, dan geldt het bieden van de mogelijkheid om de broncode van diezelfde plaats te kopiëren als het verspreiden van de broncode, zelfs indien men niet verplicht is ook de broncode te kopiëren samen met de objectcode.
</p>

<a name="section4"></a><p>
<strong>4.</strong>
 U mag het Programma niet kopiëren, wijzigen, in sublicentie geven of verspreiden op een andere wijze dan uitdrukkelijk is toegestaan onder deze Licentie. Elke andere poging om het Programma te kopiëren, te wijzigen, in sublicentie te geven of te verspreiden is nietig en zal automatisch uw rechten onder deze Licentie beëindigen. Echter, derden die van u kopieën of rechten onder deze Licentie ontvangen hebben, blijven hun rechten behouden zolang ze de voorwaarden niet schenden.
</p>

<a name="section5"></a><p>
<strong>5.</strong>
 U bent niet verplicht deze Licentie te aanvaarden, aangezien u ze niet ondertekend heeft. U heeft echter geen enkele andere toestemming om het Programma of afgeleide werken te wijzigen of te verspreiden. Deze handelingen zijn bij wet verboden als u deze Licentie niet aanvaardt. Hieruit volgt dat u aangeeft deze Licentie en al zijn voorwaarden en bepalingen in verband met het kopiëren, verspreiden of wijzigen van het Programma of erop gebaseerde werken te aanvaarden, als u het Programma (of elk op het Programma gebaseerd werk) wijzigt of verspreidt.
</p>

<a name="section6"></a><p>
<strong>6.</strong>

 Telkens u het Programma (of een op het Programma gebaseerd werk) verspreidt, krijgt de ontvanger automatisch de toelating van de originele licentiegever om het Programma te kopiëren, te verspreiden of te wijzigen onder deze bepalingen en voorwaarden. U mag de ontvanger geen andere beperkingen opleggen inzake de uitoefening van de rechten die hierin bepaald zijn. U bent niet verantwoordelijk voor het afdwingen van de naleving door derden van deze Licentie.
</p>

<a name="section7"></a><p>
<strong>7.</strong>
 Als u ten gevolge van een gerechtelijk bevel of van een beschuldiging van inbreuk op een octrooi of om eender welke andere reden (niet beperkt tot octrooikwesties) voorwaarden opgelegd worden (zij het bij gerechtelijk bevelschrift, in onderlinge overeenkomst of op een andere wijze) die in tegenspraak zijn met de voorwaarden van deze Licentie, ontslaan deze u niet van de voorwaarden van deze Licentie. Als u de verspreiding niet zodanig kunt doen dat u tegelijk voldoet aan de bepalingen van deze Licentie en aan eventuele andere toepasselijke verplichtingen, is de consequentie dat u het Programma helemaal niet mag verspreiden. Als bijvoorbeeld een octrooilicentie niet zou toestaan dat het Programma zonder royalty's verder verspreid wordt door diegenen die het Programma direct of indirect via u verkrijgen, dan zou de enige manier waarop u zowel daaraan als aan deze Licentie kunt voldoen, zijn, dat u zich volledig onthoudt van het verspreiden van het Programma.
</p>

<p>
Als een deel van dit artikel als ongeldig wordt beschouwd of ten gevolge van een specifieke omstandigheid niet kan afgedwongen worden, dan wordt de rest van het artikel van kracht geacht. In andere omstandigheden wordt het artikel in zijn geheel geacht van kracht te zijn.
</p>

<p>
Het is niet de bedoeling van dit artikel om u aan te zetten tot inbreuken tegen octrooien of tegen andere aanspraken op eigendomsrechten of tot het aanvechten van de geldigheid van zulke aanspraken. Het enige doel van dit artikel is het beschermen van de integriteit van het distributiesysteem van vrije software dat via het aanwenden van een openbare licentie gestalte krijgt. Veel mensen hebben grootmoedig bijgedragen aan een grote verscheidenheid van vrije software die via dat systeem verspreid wordt, erop vertrouwend dat dit systeem op betrouwbare wijze toegepast wordt. Het behoort tot de vrijheid van de auteur/donateur om te bepalen of hij of zij software wil verspreiden via gelijk welk ander systeem en een licentiehouder kan die keuze niet opleggen.

</p>

<p>
Dit artikel is bedoeld om dat wat verondersteld wordt een consequentie te zijn van de overige bepalingen van deze Licentie, grondig in de verf te zetten.
</p>

<a name="section8"></a><p>
<strong>8.</strong>
 Als de verspreiding en/of het gebruik van het Programma in bepaalde landen beperkt is door octrooien of door interfaces waarop copyright rust, dan mag de oorspronkelijke copyrighthouder die het Programma onder deze Licentie plaatst, een expliciete geografische beperking inzake verspreiding toevoegen waarbij deze landen uitgesloten worden, zodat verspreiding enkel toegestaan is in of tussen landen die niet aldus uitgesloten werden. In dat geval integreert deze Licentie die beperking alsof ze in het corpus van deze Licentie stond ingeschreven.
</p>

<a name="section9"></a><p>
<strong>9.</strong>
 De Free Software Foundation kan zo nu en dan herziene en/of nieuwe versies publiceren van de Algemene Publieke Licentie. Zulke nieuwe versies zullen in dezelfde geest zijn opgesteld als de huidige versie, maar kunnen in de details afwijken om nieuwe problemen of bekommernissen aan te pakken.

</p>

<p>
Elke versie krijgt een onderscheidend versienummer. Als het Programma specificeert dat een bepaalde genummerde versie van de Algemene Publieke Licentie en "elke latere versie" van toepassing is, heeft u de vrijheid om te handelen overeenkomstig de bepalingen en de voorwaarden van die genummerde versie of van elke latere versie die door de Free Software Foundation gepubliceerd werd. Als het Programma geen versienummer van de Algemene Publieke Licentie specificeert, kunt u gelijk welke versie kiezen die ooit door de Free Software Foundation werd gepubliceerd.
</p>

<a name="section10"></a><p>
<strong>10.</strong>
 Indien u delen van het Programma wilt invoegen in andere vrije programma's waarvoor andere verspreidingsvoorwaarden gelden, dan moet u de auteur van dat programma om schriftelijke toestemming vragen. Voor software waarvan het auteursrecht bij de Free Software Foundation berust moet u naar de Free Software Foundation schrijven. We maken hiervoor soms een uitzondering. Onze beslissing zal ingegeven worden door deze twee doelstellingen: de vrije status vrijwaren van alle derivaten van onze vrije software, en het delen en hergebruiken van software in het algemeen bevorderen.
</p>

<a name="section11"></a><p><strong>AFWIJZING VAN GARANTIE</strong></p>

<p>

<strong>11.</strong>
 OMDAT HET PROGRAMMA KOSTELOOS IN LICENTIE GEGEVEN WORDT, WORDEN GEEN GARANTIES GEGEVEN VOOR HET PROGRAMMA VOOR ZOVER TOEGESTAAN DOOR DE TOEPASSELIJKE REGELGEVING. BEHALVE WANNEER DIT ANDERSZINS SCHRIFTELIJK IS BEVESTIGD, BIEDEN DE AUTEURSRECHTHEBBENDEN EN/OF ANDERE PARTIJEN DIT PROGRAMMA AAN "ZOALS HET IS", ZONDER ENIGE GARANTIE, EXPLICIET OF IMPLICIET, WAARONDER MAAR NIET HIERTOE BEPERKT DE IMPLICIETE GARANTIES DIE GEBRUIKELIJK ZIJN IN DE HANDEL EN DE GARANTIE VAN BRUIKBAARHEID VOOR EEN SPECIFIEK DOEL. HET VOLLEDIGE RISICO MET BETREKKING TOT DE KWALITEIT EN DE PRESTATIES VAN HET PROGRAMMA BERUST BIJ U. MOCHT HET PROGRAMMA GEBREKEN BLIJKEN TE VERTONEN, DAN DIENT U ALLE NOODZAKELIJKE SERVICE-, REPARATIE- OF RECTIFICATIEKOSTEN VOOR EIGEN REKENING TE NEMEN.
</p>

<a name="section12"></a><p>
<strong>12.</strong>
 IN GEEN GEVAL, TENZIJ VEREIST DOOR DE TOEPASSELIJKE REGELGEVING OF SCHRIFTELIJK OVEREENGEKOMEN, ZAL ENIGE AUTEURSRECHTHEBBENDE, OF ENIGE ANDERE PARTIJ DIE HET PROGRAMMA WIJZIGT EN/OF VERSPREIDT OVEREENKOMSTIG DE HIERVOOR GEGEVEN TOESTEMMING, AANSPRAKELIJK ZIJN TEGENOVER U VOOR TOEGEBRACHTE SCHADE, INCLUSIEF ELKE ALGEMENE, SPECIALE, INCIDENTELE OF GEVOLGSCHADE DIE VOORTVLOEIT UIT HET GEBRUIK OF HET ONVERMOGEN OM HET PROGRAMMA TE GEBRUIKEN (INCLUSIEF MAAR NIET BEPERKT TOT HET VERLIES VAN DATA, DE WEERGAVE VAN INACCURATE DATA, DE DOOR U OF DERDEN GELEDEN VERLIEZEN OF EEN FALEN VAN HET PROGRAMMA OM MET ANDERE PROGRAMMA’S SAMEN TE WERKEN), ZELFS ALS EEN DERGELIJKE AUTEURSRECHTHEBBENDE OF ANDERE PARTIJ EROP GEWEZEN WERD DAT ZULKE SCHADE MOGELIJK IS.
</p>

<h3>EINDE VAN DE BEPALINGEN EN DE VOORWAARDEN</h3>

<h3><a name="howto"></a><a name="SEC4">Hoe u deze bepalingen kunt toepassen op uw nieuwe programma’s</a></h3>

<p>
  Als u een nieuw programma ontwikkelt en u wenst dat het van het grootst mogelijke nut is voor het publiek, kunt u dit het best bereiken door het vrije software te maken die door iedereen verder kan worden verspreid en gewijzigd onder deze voorwaarden.
</p>

<p>
  Dat doet u door de volgende kennisgeving aan het programma toe te voegen. Het is het veiligste om ze aan het begin van elk broncodebestand op te nemen om zo op de meest effectieve wijze elke aansprakelijkheid uit te sluiten; en elk bestand zou tenminste de "copyright"-regel moeten bevatten en een verwijzing naar waar de volledige kennisgeving gevonden kan worden.
</p>
<hr />
<p>
  <b>Opmerking</b>: enkel de oorspronkelijke Engelse tekst van de kennisgeving heeft bindende waarde en niet de hiernavolgende vertaling ervan.
</p>
<hr />

<pre>
<var>een regel om de naam van het programma weer te geven en een korte omschrijving van wat het programma doet.</var>
Copyright (C) <var>yyyy</var>  <var>naam van de auteur</var>

Dit programma is vrije software; u mag het verder verspreiden en/of
wijzigen onder de voorwaarden van de GNU Algemene Publieke Licentie
zoals gepubliceerd door de Free Software Foundation, ofwel versie 2
van de Licentie of (naar uw keuze) elke latere versie.

Dit programma wordt gedistribueerd in de hoop dat het nuttig zal zijn,
maar ZONDER ENIGE GARANTIE, zelfs zonder de impliciete garanties die
GEBRUIKELIJK ZIJN IN DE HANDEL OF DE GARANTIE VAN BRUIKBAARHEID VOOR EEN
SPECIFIEK DOEL. Zie de GNU Algemene Publieke Licentie voor meer details.

U zou samen met dit programma een kopie van de GNU Algemene Publieke Licentie
ontvangen moeten hebben. Is dit niet het geval, schrijf dan naar de Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
</pre>

<p>
Voeg ook informatie toe over hoe u te bereiken bent via e-mail en briefpost.
</p>

<p>
Indien het een interactief programma betreft, laat het dan bij het opstarten
in interactieve modus een korte melding weergeven in de volgende zin:
</p>

<pre>
Gnomovision versie 69, Copyright (C) <var>jaar</var> <var>naam van de auteur</var>

Gnomovision komt met ABSOLUUT GEEN ENKELE GARANTIE; voor meer details
typt u `toon g'.  Dit is vrije software en u bent welkom om
deze onder bepaalde voorwaarden verder te verspreiden; typ `toon c' 
voor details.
</pre>

<p>
De hypothetische commando's <samp>`toon g'</samp> en <samp>`toon c'</samp>
moeten de overeenkomstige delen van de Algemene Publieke Licentie tonen.
Uiteraard mogen de door u gebruikte commando's anders heten dan <samp>`toon g'</samp> en
<samp>`toon c'</samp>; het kan zelfs ook om een klik met de muis gaan of items
in een menu  &mdash; afhankelijk van wat het beste bij uw programma past.
</p>

<p>
U moet eventueel ook uw werkgever (als u als programmeur tewerkgesteld bent)
of uw school, indien nodig, een "afwijzing van copyright" voor het programma laten ondertekenen. Hierna volgt een voorbeeld; pas de namen aan:
</p>


<pre>
Yoyodyne nv verklaart hierbij geen auteursrechten
op te eisen voor het programma `Gnomovision'
(dat compilatieprogramma's avances maakt), 
geschreven door Jan Ontwikkelaar.

<var>handtekening van Piet Ondernemer</var>, 1 april 1989
Piet Ondernemer, vicevoorzitter
</pre>
