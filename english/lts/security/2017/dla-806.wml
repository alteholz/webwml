<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Information disclosure and authentication bypass vulnerability exists in
the Apache HTTP Server configuration bundled with ZoneMinder v1.30.0,
which allows a remote unauthenticated attacker to browse all directories
in the web root, e.g., a remote unauthenticated attacker can view all
CCTV images on the server.</p>

<p>For new installations, the new config file will be automatically
installed. For existing installations, please follow the instructions in
NEWS, which will be viewed during upgrade.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.25.0-4+deb7u1.</p>

<p>We recommend that you upgrade your zoneminder packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-806.data"
# $Id: $
