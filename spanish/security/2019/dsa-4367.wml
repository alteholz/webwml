#use wml::debian::translation-check translation="9b882a5d38bd6c3c5a8d79a39b0033bc189699c0"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Los laboratorios de investigación Qualys («Qualys Research Labs») descubrieron múltiples vulnerabilidades en
systemd-journald. Dos defectos de corrupción de memoria por medio de asignaciones controladas por
el atacante utilizando la función alloca (<a href="https://security-tracker.debian.org/tracker/CVE-2018-16864">CVE-2018-16864</a>, 
<a href="https://security-tracker.debian.org/tracker/CVE-2018-16865">CVE-2018-16865</a>)
y un defecto de lectura fuera de límites que daba lugar a una filtración de información
(<a href="https://security-tracker.debian.org/tracker/CVE-2018-16866">CVE-2018-16866</a>)
podían permitir que un atacante provocara denegación de servicio o ejecución de
código arbitrario.</p>

<p>Más detalles en el aviso de seguridad de Qualys:
<a href="https://www.qualys.com/2019/01/09/system-down/system-down.txt">https://www.qualys.com/2019/01/09/system-down/system-down.txt</a></p>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 232-25+deb9u7.</p>

<p>Le recomendamos que actualice los paquetes de systemd.</p>

<p>Para información detallada sobre el estado de seguridad de systemd, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/systemd">https://security-tracker.debian.org/tracker/systemd</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4367.data"
